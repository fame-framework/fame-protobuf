<!-- SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->
# Upgrading

## [2.0.0]
### Breaking Changes
This version of fame-protobuf requires fame-io of at least version 3.0 and fame-core of at least version 2.0.
Thus, update your python environment and fame-core dependencies accordingly.