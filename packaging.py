# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import os
import toml
import fileinput
import xml.etree.ElementTree as ElementTree

PACKAGE_DIR = "src/main/python/fameprotobuf/"


def generate_init():
    """Generates an empty `__init__.py` file in the `fameprotobuf` folder and passes if already existent"""
    try:
        file = open(PACKAGE_DIR + "__init__.py", "x")
        file.close()
    except FileExistsError:
        pass


def adapt_imports():
    """Changes imports in fameprotobuf from `import ...` to `from fameprotobuf import ...`"""
    for file_name in os.listdir(PACKAGE_DIR):
        with fileinput.FileInput(os.path.join(PACKAGE_DIR, file_name), inplace=True) as file:
            for line in file:
                if "import" in line and "_pb2" in line and "from fameprotobuf" not in line:
                    line = line.replace("import", "from fameprotobuf import")
                print(line, end='')


def update_version_in_toml(version: str) -> None:
    """Sets package version to given value if not already set"""
    project = toml.load("pyproject.toml")
    if project["tool"]["poetry"]["version"] != version:
        print(f"Updating version to: {version}")
        project["tool"]["poetry"]["version"] = version
        with open("pyproject.toml", 'w') as file:
            toml.dump(project, file)
    else:
        print("Version already up to date!")


def ensure_version_compiled(version: str) -> None:
    """Ensures that in target/ folder a file ending with `version_string`.jar exists"""
    file_path = "./target/protobuf-" + version + ".jar"
    if not os.path.isfile(file_path):
        raise Exception("Could not find compiled java files for version '{}'. Compile project first!".format(version))


def get_version_from_pom() -> str:
    """Returns version from pom.xml"""
    pom = ElementTree.parse('pom.xml')
    version = pom.find('.//{http://maven.apache.org/POM/4.0.0}version').text
    if not version:
        raise Exception("Could not extract version from pom.xml")
    else:
        print("Building for version number: " + version)
        return version


if __name__ == '__main__':
    version_string = get_version_from_pom()
    ensure_version_compiled(version_string)
    update_version_in_toml(version_string)
    generate_init()
    adapt_imports()
