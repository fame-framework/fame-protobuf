<!-- SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->
# Welcome
Welcome fellow researcher.
We are happy that you want to contribute to and improve FAME-Protobuf.

## General information
FAME is a tool that is open for your contribution.
It is an interdisciplinary, open source project, thus help is always appreciated!
The development happens openly on GitLab and is supplemented by online developer meetings to discuss more complicated topics.

### Release plan
A detailed planning process regarding releases is not yet defined.
Minor bug fixes and new features will be continuously released.

## What is where?
Here you can find information on how to contribute.
For an overview of FAME in general please visit the [FAME-Wiki](https://gitlab.com/fame-framework/wiki/-/wikis/home).
There, you can also find relevant background information like coding conventions, a glossary and code design elements.

Installation instructions can be found in the [ReadMe](README.md) file.

Issues and Bugs are tracked on the [Repository](https://gitlab.com/fame-framework/fame-protobuf/-/issues) at GitLab.

There is no public forum, yet. 

## Contact / Help
You can contact the main developers via [fame@dlr.de](mailto:fame@dlr.de) to pose any questions or to get involved.

# How to Contribute
You are welcome to contribute to FAME via bug reports and feature requests at any time.
Please note that code contributions (e.g. via pull requests) require you to fill in the [Contributor License Agreement](CLA.md), before we can merge your code into the project.

## Feature request
1. Please check the open issue list to see whether an issue similar to your feature idea already exists.
2. If no similar issue exists: Open a new issue using the *feature_request* template and fill in the corresponding items.
3. You may contact the main developers via [fame@dlr.de](mailto:fame@dlr.de) to discuss the issue with us.

## Bug report
1. Please check the open issue list to see whether a similar bug has already been reported.
2. If no similar bug was reported yet: Open a new issue using the *bug_report* template and fill in the corresponding items.
3. You may contact the main developers via [fame@dlr.de](mailto:fame@dlr.de) to discuss the issue with us.

## Code contributions
Thank you for your intention to contribute.

### Environment
We recommend Java 11 (64 bit) to run FAME applications and rely on [Maven](https://maven.apache.org/) for building the stack.

### Coding
Please follow the official Protobuf [Style Guide](https://protobuf.dev/programming-guides/style/).

### Before submitting
Please, check the following points before submitting a pull request:
1. Please fill in the [Contributor License Agreement](CLA.md) and send it to [fame@dlr.de](mailto:fame@dlr.de).
1. Ensure there is a corresponding issue to your code contribution.
1. Make sure your code is based on the latest version of the *dev* branch and that there are no conflicts. In case of conflicts: fix them first.
1. Make sure that existing unit tests are all successful. Add new unit tests that cover your code contributions and ensure that your code works as intended. In case an error occurred that you don't know how to solve, ask for help from the main developers.
1. Update the version number of the project in `pom.xml`. Follow conventions of [semantic versioning 2.0.0](https://semver.org/).
1. Update `CHANGELOG.md` reflecting on the code changes made. Follow the changelog [style guide](https://github.com/vweevers/common-changelog).
1. If breaking changes occur: add hints on how to upgrade to `UPGRADING.md`.

### Pull request
1. Submit your request using the provided *pull_request* template & (briefly) describe the changes you made in the pull request.
1. Contact the main developers via [fame@dlr.de](mailto:fame@dlr.de) in case you have got any questions.

# List of Contributors
In order of their first contribution

* Christoph Schimeczek
* Marc Deissenroth-Uhrig
* Ulrich Frey
* Benjamin Fuchs
* Felix Nitsch
* A. Achraf El Ghazi
